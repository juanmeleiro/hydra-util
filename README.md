# Hydra-util

Hydra-util, from the mythical creature, the Hydra, is the ultimate bash-utility you didn't need. It's behaviour is simple: if created, go underground (_i.e._, to background). If killed, it forks itself twice and dies.

## Options

- `-h | --help` displays help message
- `-v | --version` displays version information

## Todo

- [Standards](https://www.gnu.org/prep/standards/standards.html#Source-Language) complience
